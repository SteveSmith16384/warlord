package com.scs.warlord.components;

import ssmith.android.compatibility.RectF;


public interface ICollideable {

	RectF getRect();
	
	void collided(ICollideable other);

	void moveBy(float x, float y);

}
