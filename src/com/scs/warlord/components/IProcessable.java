package com.scs.warlord.components;

public interface IProcessable {

	public void process(long interpol);
	
}
