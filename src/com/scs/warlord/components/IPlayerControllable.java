package com.scs.warlord.components;

public interface IPlayerControllable {

	void processInput(long interpol);
	
}
