package com.scs.warlord.components;

import ssmith.android.compatibility.Canvas;

public interface IDrawable {

	void doDraw(Canvas g, long interpol);

}
