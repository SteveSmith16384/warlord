package com.scs.warlord.input;

import org.gamepad4j.DpadDirection;
import org.gamepad4j.IController;
import org.gamepad4j.StickID;
import org.gamepad4j.StickPosition;

public final class PS4Controller implements IInputDevice {

	private IController gamepad;

	public PS4Controller(IController _gamepad) {
		gamepad = _gamepad;
	}


	@Override
	public boolean isUpPressed() {
		StickPosition pos = gamepad.getStick(StickID.LEFT).getPosition();
		return pos.getDirection() == DpadDirection.UP;
	}


	@Override
	public boolean isDownPressed() {
		StickPosition pos = gamepad.getStick(StickID.LEFT).getPosition();
		//return pos.getDirection() == DpadDirection.DOWN;
		return pos.getDegree() > 107 && pos.getDegree() < 252;
	}


	@Override
	public float getStickDistance() {
		StickPosition pos = gamepad.getStick(StickID.LEFT).getPosition();
		/*if (Statics.DEBUG) {
			Statics.p("Dist=" + pos.getDistanceToCenter());
		}*/
		return pos.getDistanceToCenter();
	}

	
	@Override
	public int getID() {
		return gamepad.getDeviceID();
	}

}
