package com.scs.warlord.input;

public interface NewControllerListener {

	void newController(IInputDevice input);

}
