package com.scs.warlord.input;

public interface IInputDevice {

	int getID();
	
	boolean isUpPressed();
	
	boolean isDownPressed();
	
	float getStickDistance();
	
}
