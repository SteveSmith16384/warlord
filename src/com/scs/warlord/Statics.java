package com.scs.warlord;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;

import ssmith.android.compatibility.Typeface;
import ssmith.android.framework.AbstractActivity;
import ssmith.awt.ImageCache;

public final class Statics {

	public static final boolean FULL_SCREEN = false;
	public static final boolean HIDE_GFX = true;
	public static final boolean DEBUG = true;
	public static final boolean RELEASE_MODE = false; // Stricter if false
	public static final String VERSION_NAME = "0.1";

	// Sides
	public static final int LOOP_DELAY = 30;
	public static final String EMAIL = "help@penultimateapps.com"; 
	public static final float LABEL_SPACING = 1.2f;
	public static final int WINDOW_TOP_OFFSET = 25;
	public static String BACKGROUND_IMAGE = null;

	public static AbstractActivity act;
	public static Typeface stdfnt, iconfnt, bigfnt;
	public static ImageCache img_cache;


	// Bitmap scales
	public static String NAME;
	public static float SCREEN_WIDTH, SCREEN_HEIGHT;
	public static float PLAYER_WIDTH, PLAYER_HEIGHT;
	public static float PLAYER_SPEED;
	public static float BRICK_WIDTH, BRICK_HEIGHT;
	public static float BALL_DIAM, BALL_SPEED;
	public static float POWERUP_SIZE;
	public static float BARRIER_SIZE;
	public static float PLAYER0_X, PLAYER1_X;

	public static void init(AbstractActivity _act) {
		if (FULL_SCREEN) {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			SCREEN_WIDTH = (int)screenSize.getWidth();
			SCREEN_HEIGHT = (int)screenSize.getHeight();
		} else {
			SCREEN_WIDTH = 800;
			SCREEN_HEIGHT = 600;
		}

		PLAYER_WIDTH = SCREEN_WIDTH / 50;
		PLAYER_HEIGHT = SCREEN_HEIGHT / 10;
		PLAYER_SPEED = SCREEN_WIDTH / 100; //9f;
		BALL_SPEED = PLAYER_SPEED /2;
		BRICK_WIDTH = SCREEN_WIDTH / 50;
		BRICK_HEIGHT = SCREEN_HEIGHT / 15;
		BALL_DIAM = SCREEN_WIDTH / 50;
		POWERUP_SIZE = SCREEN_WIDTH / 30;
		BARRIER_SIZE = SCREEN_WIDTH / 20;
		PLAYER0_X = Statics.SCREEN_WIDTH * 0.15f;
		PLAYER1_X = Statics.SCREEN_WIDTH - PLAYER0_X;//* 0.8f;

		// Load font
		/*try {
				InputStream fntStr = Statics.class.getClassLoader().getResourceAsStream("fonts/SF Distant Galaxy.ttf");
				GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
				ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, fntStr));
				fntStr.close();
				iconfnt = new Typeface("SF Distant Galaxy", Font.PLAIN, 10);
				stdfnt = new Typeface("SF Distant Galaxy", Font.BOLD, 14);
				bigfnt = new Typeface("SF Distant Galaxy", Font.BOLD, 28);
			} catch (Exception e) {
				e.printStackTrace();*/
		// just use helvetica
		iconfnt = new Typeface("Helvetica", Font.PLAIN, 10);
		stdfnt = new Typeface("Helvetica", Font.BOLD, 14);
		bigfnt = new Typeface("Helvetica", Font.BOLD, 28);

	}


	// ----------------------------------------------------------------------

	public static void p(String s) {
		System.out.println(s);
	}


	public static void pe(String s) {
		System.err.println(s);
	}

}
