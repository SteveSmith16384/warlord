package com.scs.warlord.entities;

import java.awt.Color;

import ssmith.android.compatibility.Canvas;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.components.IProcessable;
import com.scs.warlord.game.GameModule;

public class PowerUp extends PhysicalEntity implements IProcessable, ICollideable, IDrawable {

	public PowerUp(GameModule _game, float x, float y) {
		super(_game, "Ball", x, y, Statics.POWERUP_SIZE, Statics.POWERUP_SIZE);
	}
	

	@Override
	public void doDraw(Canvas g, long interpol) {
		g.getGraphics().setColor(Color.green);
		g.getGraphics().fillRect((int)rect.left, (int)rect.top, (int)rect.width(), (int)rect.height());
		
	}

	
	@Override
	public void collided(ICollideable other) {
		if (other instanceof PlayersPaddle) {
			// todo
		}
		this.game.removeEntity(this);
		
	}
	

	@Override
	public void moveBy(float x, float y) {
		// Do nothing
		
	}
	

	@Override
	public void process(long interpol) {
		// TODO  - Move
		
	}

}
