package com.scs.warlord.entities;

import java.awt.image.BufferedImage;

import ssmith.android.compatibility.Canvas;
import ssmith.lang.NumberFunctions;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.game.GameModule;

public class Brick extends PhysicalEntity implements ICollideable, IDrawable {

	private BufferedImage img;

	public Brick(GameModule _game, float x, float y) {
		super(_game, "Brick", x, y, Statics.BRICK_WIDTH, Statics.BRICK_HEIGHT);

		switch (NumberFunctions.rnd(1, 4)) {
		case 1:
			img = Statics.img_cache.getImage("menu_frame_blue.png", Statics.BRICK_WIDTH, Statics.BRICK_HEIGHT);
			break;

		case 2:
			img = Statics.img_cache.getImage("menu_frame_green.png", Statics.BRICK_WIDTH, Statics.BRICK_HEIGHT);
			break;

		case 3:
			img = Statics.img_cache.getImage("menu_frame_red.png", Statics.BRICK_WIDTH, Statics.BRICK_HEIGHT);
			break;

		case 4:
			img = Statics.img_cache.getImage("menu_frame_yellow.png", Statics.BRICK_WIDTH, Statics.BRICK_HEIGHT);
			break;
}
	}


	@Override
	public void doDraw(Canvas g, long interpol) {
		//g.getGraphics().setColor(Color.white);
		//g.getGraphics().fillRect((int)rect.left, (int)rect.top, (int)rect.width(), (int)rect.height());

		g.getGraphics().drawImage(img, (int)rect.left, (int)rect.top, null);
	}


	@Override
	public void collided(ICollideable other) {
		game.removeEntity(this);

	}


	@Override
	public void moveBy(float x, float y) {
		// Do nothing

	}


}
