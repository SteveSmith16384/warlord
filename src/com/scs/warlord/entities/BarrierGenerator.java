package com.scs.warlord.entities;

import ssmith.lang.Functions;

import com.scs.warlord.Statics;
import com.scs.warlord.components.IProcessable;
import com.scs.warlord.game.GameModule;

public class BarrierGenerator extends Entity implements IProcessable {

	private static final long INTERVAL = 14 * 1000;
	private static final int MAX_BARRIERS = 20;

	private long timeUntil = INTERVAL;
	private int numBarriers = 0;

	public BarrierGenerator(GameModule _game) {
		super(_game, "BarrierGenerator");
	}

	@Override
	public void process(long interpol) {
		if (numBarriers < MAX_BARRIERS) {
			timeUntil -= interpol;
			if (timeUntil < 0) {
				int x = (int)Functions.rndFloat(Statics.PLAYER0_X*1.1f, Statics.PLAYER1_X*0.9f);
				// Check not in middle
				while (x > (Statics.SCREEN_WIDTH/2)-Statics.BALL_DIAM && x < (Statics.SCREEN_WIDTH/2)+Statics.BALL_DIAM) {
					x = (int)Functions.rndFloat(Statics.PLAYER0_X, Statics.PLAYER1_X);
				}
				int y = Functions.rnd(0, (int)Statics.SCREEN_HEIGHT);
				float w = Functions.rndFloat(Statics.BARRIER_SIZE/2, Statics.BARRIER_SIZE*1.5f);
				float h = Functions.rndFloat(Statics.BARRIER_SIZE/2, Statics.BARRIER_SIZE*1.5f);
				Barrier b = new Barrier(game, x, y, w, h);
				// Check it's not over a ball
				if (game.isAreaClear(b.rect)) {
					game.addEntity(b);
					numBarriers++;
					timeUntil = INTERVAL;
				}
			}
		}
	}

}
