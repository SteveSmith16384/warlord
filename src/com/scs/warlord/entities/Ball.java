package com.scs.warlord.entities;

import java.awt.Point;
import java.awt.image.BufferedImage;

import ssmith.android.compatibility.Canvas;
import ssmith.android.compatibility.PointF;
import ssmith.lang.Functions;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.components.IProcessable;
import com.scs.warlord.game.GameModule;

public class Ball extends PhysicalEntity implements IProcessable, ICollideable, IDrawable {

	private Point overallMoveDir;// = new Point(1, 1);
	private boolean moveLR;
	private PointF prevPos = new PointF();
	private BufferedImage img;

	public Ball(GameModule _game) {//, float x, float y) {
		super(_game, "Ball", Statics.SCREEN_WIDTH/2, Statics.SCREEN_HEIGHT/2, Statics.BALL_DIAM, Statics.BALL_DIAM);
		
		img = Statics.img_cache.getImage("ball.png", Statics.BALL_DIAM, Statics.BALL_DIAM);

		int z = Functions.rnd(1,  4);
		switch (z) {
		case 1:
			overallMoveDir = new Point(1, 1);
			break;
		case 2:
			overallMoveDir = new Point(-1, 1);
			break;
		case 3:
			overallMoveDir = new Point(1, -1);
			break;
		case 4:
			overallMoveDir = new Point(-1, -1);
			break;
		}
	}

	
	@Override
	public void collided(ICollideable other) {
		this.bounce();
		
	}
	

	private void bounce() {
		//Statics.p("BOUNCE!");
		// Move back
		this.rect.left = prevPos.x;
		this.rect.right = this.rect.left + Statics.BALL_DIAM;
		this.rect.top = prevPos.y;
		this.rect.bottom = this.rect.top + Statics.BALL_DIAM;
		
		// Change dir
		if (moveLR) {
			this.overallMoveDir.x = this.overallMoveDir.x * -1; 
		} else {
			this.overallMoveDir.y = this.overallMoveDir.y * -1; 
		}
	}
	
	
	@Override
	public void process(long interpol) {
		// Left/right first
		moveLR = true;
		prevPos.x = this.rect.left; 
		prevPos.y = this.rect.top; 
		this.rect.left += (overallMoveDir.x * Statics.BALL_SPEED);
		this.rect.right = this.rect.left + Statics.BALL_DIAM;
		
		if (this.rect.left < 0) {
			game.playerWon(0);
			//bounce();
		} else if (this.rect.right > Statics.SCREEN_WIDTH) {
			game.playerWon(1);
			//bounce();
		} else {
			this.checkCollisions();
		}

		// Up/down
		moveLR = false;
		prevPos.x = this.rect.left; 
		prevPos.y = this.rect.top; 
		this.rect.top += (overallMoveDir.y * Statics.BALL_SPEED);
		this.rect.bottom = this.rect.top + Statics.BALL_DIAM;
		
		if (this.rect.top < 0) {
			bounce();
		} else if (this.rect.bottom > Statics.SCREEN_HEIGHT) {
			bounce();
		} else {
			this.checkCollisions();
		}

	}

	
	@Override
	public void doDraw(Canvas g, long interpol) {
		/*g.getGraphics().setColor(Color.white);
		g.getGraphics().fillOval((int)rect.centerX()-rad, (int)rect.centerY()-rad, (int)rect.width(), (int)rect.height());*/

		g.getGraphics().drawImage(img, (int)rect.left, (int)rect.top, null);

	}


	@Override
	public void moveBy(float x, float y) {
		this.rect.top += x;
		this.rect.bottom += x;
		this.rect.left += y;
		this.rect.right += y;
		
	}

}
