package com.scs.warlord.entities;

import java.awt.image.BufferedImage;

import ssmith.android.compatibility.Canvas;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.components.IPlayerControllable;
import com.scs.warlord.game.GameModule;
import com.scs.warlord.game.Player;

public class PlayersPaddle extends PhysicalEntity implements IPlayerControllable, ICollideable, IDrawable {

	private Player player;
	private float moveDist;
	private BufferedImage img;

	public PlayersPaddle(Player _player, GameModule _game, float x) {
		super(_game, "Paddle", x, Statics.SCREEN_HEIGHT/2, Statics.PLAYER_WIDTH, Statics.PLAYER_HEIGHT);

		player = _player;
		img = Statics.img_cache.getImage("bricks.png", Statics.PLAYER_WIDTH, Statics.PLAYER_HEIGHT);
	}


	@Override
	public void collided(ICollideable other) {
		//other.moveBy(0, moveDist); // Push other out the way
		//this.moveBy(0,  -moveDist); // Move us back
	}


	@Override
	public void processInput(long interpol) {
		moveDist = 0;
		if (this.player.input.isUpPressed()) {
			moveDist = -Statics.PLAYER_SPEED;
			this.rect.top -= Statics.PLAYER_SPEED;
			this.rect.bottom -= Statics.PLAYER_SPEED;
			if (this.rect.top < 0) {
				this.rect.top = 0;
				this.rect.bottom = Statics.PLAYER_HEIGHT;
			}
			this.checkCollisions();
		} else if (this.player.input.isDownPressed()) {
			moveDist = -Statics.PLAYER_SPEED;
			this.rect.top += Statics.PLAYER_SPEED;
			this.rect.bottom += Statics.PLAYER_SPEED;
			if (this.rect.bottom > Statics.SCREEN_HEIGHT) {
				this.rect.top = Statics.SCREEN_HEIGHT - Statics.PLAYER_HEIGHT;
				this.rect.bottom = Statics.SCREEN_HEIGHT;
			}
			this.checkCollisions();
		}

	}


	@Override
	public void doDraw(Canvas g, long interpol) {
		//g.getGraphics().setColor(Color.white);
		//g.getGraphics().fillRect((int)rect.left, (int)rect.top, (int)rect.width(), (int)rect.height());

		g.getGraphics().drawImage(img, (int)rect.left, (int)rect.top, null);
	}


	@Override
	public void moveBy(float x, float y) {
		// Do nothing

	}


}
