package com.scs.warlord.entities;

import com.scs.warlord.components.IProcessable;
import com.scs.warlord.game.GameModule;

public class SecondBallEvent extends Entity implements IProcessable {
	
	private static final int INTERVAL = 30 * 1000;
	
	private long timeUntil = INTERVAL;
	private long numBalls = 0;

	public SecondBallEvent(GameModule _game) {
		super(_game, "SecondBallEvent");
	}
	

	@Override
	public void process(long interpol) {
		timeUntil -= interpol;
		if (timeUntil < 0) {
			game.addEntity(new Ball(game));
			game.removeEntity(this);
			timeUntil = INTERVAL;
			numBalls++;
		}
		
	}

}
