package com.scs.warlord.entities;

import com.scs.warlord.game.GameModule;


public abstract class Entity {

	protected GameModule game;
	public String name;

	public Entity(GameModule _game, String _name) {
		super();

		game = _game;
		name = _name;
	}

	
	@Override
	public String toString() {
		return name;
	}

}

