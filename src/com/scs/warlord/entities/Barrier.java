package com.scs.warlord.entities;

import java.awt.image.BufferedImage;

import ssmith.android.compatibility.Canvas;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.game.GameModule;

public class Barrier extends PhysicalEntity implements ICollideable, IDrawable {

	private BufferedImage img;
	
	public Barrier(GameModule _game, float x, float y, float w, float h) {
		super(_game, "Barrier", x, y, w, h);//Statics.BARRIER_SIZE, Statics.BARRIER_SIZE);

		img = Statics.img_cache.getImage("crate.gif", w, h);
	}

	
	@Override
	public void doDraw(Canvas g, long interpol) {
		//g.getGraphics().setColor(Color.white);
		//g.getGraphics().fillRect((int)rect.left, (int)rect.top, (int)rect.width(), (int)rect.height());

		g.getGraphics().drawImage(img, (int)rect.left, (int)rect.top, null);
	}
	

	@Override
	public void collided(ICollideable other) {
		// Do nothing
	}

	
	@Override
	public void moveBy(float x, float y) {
		// Do nothing
		
	}


}
