package com.scs.warlord.entities;

import ssmith.android.compatibility.RectF;

import com.scs.warlord.components.ICollideable;
import com.scs.warlord.game.GameModule;

public class PhysicalEntity extends Entity {

	protected RectF rect;

	public PhysicalEntity(GameModule _game, String _name, float x, float y, float w, float h) {
		super(_game, _name);

		rect = new RectF(x, y, x+w, y+h);
	}


	//public List<ICollideable> getColliders() {
	public void checkCollisions() {
		ICollideable me = (ICollideable)this;
		//List<ICollideable> ret = new ArrayList<>();
		synchronized (game.entities) {
			for (Entity e : game.entities) {
				if (e instanceof ICollideable) {
					if (e != this) {
						ICollideable other = (ICollideable)e;
						if (RectF.intersects(me.getRect(), other.getRect())) {
							me.collided(other);
							other.collided(me);
						}
						//ret.add((ICollideable)e);
					}
				}
			}
		}

		//return ret;
	}


	public RectF getRect() {
		return rect;
	}

}
