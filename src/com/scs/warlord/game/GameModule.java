package com.scs.warlord.game;

import java.awt.Color;

import org.gamepad4j.Controllers;

import ssmith.android.compatibility.Canvas;
import ssmith.android.compatibility.RectF;
import ssmith.android.framework.AbstractActivity;
import ssmith.android.framework.MyEvent;
import ssmith.android.framework.modules.AbstractModule;
import ssmith.util.IDisplayText;
import ssmith.util.TSArrayList;

import com.scs.warlord.Statics;
import com.scs.warlord.components.ICollideable;
import com.scs.warlord.components.IDrawable;
import com.scs.warlord.components.IPlayerControllable;
import com.scs.warlord.components.IProcessable;
import com.scs.warlord.entities.Ball;
import com.scs.warlord.entities.BarrierGenerator;
import com.scs.warlord.entities.Brick;
import com.scs.warlord.entities.Entity;
import com.scs.warlord.entities.PlayersPaddle;
import com.scs.warlord.entities.SecondBallEvent;
import com.scs.warlord.input.DeviceThread;
import com.scs.warlord.start.StartupModule;

public final class GameModule extends AbstractModule implements IDisplayText {

	public TSArrayList<Entity> entities = new TSArrayList<Entity>();
	private String currentMsg = null;

	public GameModule(AbstractActivity act) { 
		super(act, null);

		this.mod_return_to = new StartupModule(act);

		this.stat_cam.lookAtTopLeft(true);
		this.root_cam.lookAtTopLeft(true);

		this.setBackground(Statics.BACKGROUND_IMAGE);

		if (this.getThread().players.size() == 2) {
			this.startGame();
		} else {
			currentMsg = "Press UP to join game!";
		}

	}


	private void startGame() {
		synchronized (entities) {
			entities.clear();
		}

		this.root_node.detachAllChildren();
		this.stat_node_back.detachAllChildren();
		this.stat_node_front.detachAllChildren();

		// Create players
		synchronized (this.getThread().players) {
			for (Player player : this.getThread().players.values()) {
				PlayersPaddle paddle = null;
				if (player.numZB == 0) {
					paddle = new PlayersPaddle(player, this, Statics.PLAYER0_X);
				} else if (player.numZB == 1) {
					paddle = new PlayersPaddle(player, this, Statics.PLAYER1_X);
				}
				this.addEntity(paddle);
				createBricks(player.numZB);
			}
		}

		Ball ball = new Ball(this);
		this.addEntity(ball);

		SecondBallEvent sbe = new SecondBallEvent(this);
		this.addEntity(sbe);

		BarrierGenerator bg = new BarrierGenerator(this);
		this.addEntity(bg);

		this.currentMsg = "Game started";
	}


	private void createBricks(int playerZB) {
		for (int y=0 ; y < Statics.SCREEN_HEIGHT ; y+=Statics.BRICK_HEIGHT+1) {
			for (int x=0 ; x<3 ; x++) {
				int x2 = (int)(x*Statics.BRICK_WIDTH)+x;
				if (playerZB == 1) {
					x2 = (int)(Statics.SCREEN_WIDTH - x2 - x - Statics.BRICK_WIDTH);
				}
				Brick brick = new Brick(this, x2+x, y);
				this.addEntity(brick);
			}
		}
	}


	@Override
	public boolean processEvent(MyEvent ev) throws Exception {
		try {
			// Do nothing
		} catch (RuntimeException ex) {
			AbstractActivity.HandleError(null, ex);
		}
		return false;
	}


	public void updateGame(long interpol) {
		if (DeviceThread.USE_CONTROLLERS) {
			Controllers.checkControllers();
		}

		// Remove any objects marked for removal
		synchronized (entities) {
			this.entities.refresh();
		}

		if (this.getThread().players.size() == 2) {
			// Player input first
			for (Entity e : this.entities) {
				if (e instanceof IPlayerControllable) {
					IPlayerControllable id = (IPlayerControllable)e;
					id.processInput(interpol);
				}
			}

			// Process
			synchronized (entities) {
				for (Entity e : this.entities) {
					if (e instanceof IProcessable) {
						IProcessable id = (IProcessable)e;
						id.process(interpol);
					}
				}
			}
		}

	}


	@Override
	public void doDraw(Canvas c, long interpol) {
		if (currentMsg != null) {
			c.getGraphics().setColor(Color.white);
			c.getGraphics().drawString(this.currentMsg, 50, 30);
		}

		synchronized (entities) {
			for (Entity e : this.entities) {
				if (e instanceof IDrawable) {
					IDrawable id = (IDrawable)e;
					id.doDraw(c, interpol);
				}
			}
		}

	}


	public void addEntity(Entity o) {
		this.entities.add(o);
	}


	public void removeEntity(Entity o) {
		this.entities.remove(o);
	}


	@Override
	public boolean onBackPressed() {
		this.returnTo();
		return true;
	}


	@Override
	public void displayText(String s) {
		this.currentMsg = s;
	}


	@Override
	public void newPlayer(Player player) {
		this.currentMsg = "Player " + player.numZB + " joined";
		if (this.getThread().players.size() == 2) {
			this.startGame();
		}
	}


	public void playerWon(int numZB)  {
		this.currentMsg = "Player " + (1+numZB) + " has won!";
		this.getThread().players.clear();
		DeviceThread.ClearDevices();
	}


	public boolean isAreaClear(RectF rect) {
		synchronized (entities) {
			for (Entity e : entities) {
				if (e instanceof ICollideable) {
					ICollideable other = (ICollideable)e;
					if (RectF.intersects(rect, other.getRect())) {
						return false;
					}
				}
			}
		}
		return true;
	}

}

