package com.scs.warlord.game;

import com.scs.warlord.input.IInputDevice;

public final class Player {

	public int numZB, score;
	public IInputDevice input;
	
	public Player(IInputDevice _input, int _num) {
		super();
		
		input = _input;
		numZB = _num;
	}

}
