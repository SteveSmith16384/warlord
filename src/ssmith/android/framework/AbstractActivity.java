package ssmith.android.framework;

import ssmith.audio.MP3Player;

import com.scs.warlord.MainThread;
import com.scs.warlord.Statics;
import com.scs.warlord.sfx.ISfxPlayer;
import com.scs.warlord.sfx.StdSfxPlayer;


public abstract class AbstractActivity implements Thread.UncaughtExceptionHandler {

	public static MainThread thread; // thread must be here as this is the only constant class
	public static ISfxPlayer sound_manager;
	private MP3Player mp3Music;

	public AbstractActivity() {
		super();

	}


	public void onCreate() {
		Statics.init(this);
		Statics.act = this;

		Thread.setDefaultUncaughtExceptionHandler(this);

		if (thread != null) {
			if (thread.isAlive() == false) {
				thread = null;
			}
		}
		if (thread == null) {
			thread = new MainThread();//getHolder(), super.getContext(), this);
			thread.setRunning(true);
			thread.start();
		}

		if (sound_manager == null) {
			sound_manager = new StdSfxPlayer("assets/sfx/");
			//sound_manager.playSound("Venus.ogg");
		}

		//mp3Music = new MP3Player("assets/music/Venus.ogg", true);
		//mp3Music.start();

	}
	
	
	public static void HandleError(AbstractActivity act, Throwable ex) {
		HandleError(ex);
	}

	
	public static void HandleError(Throwable ex) {
		try {
			ex.printStackTrace();
		} catch (Exception ex2) {
			ex2.printStackTrace();
		}
	}


	@Override
	public void uncaughtException(Thread thread, Throwable ex) {
		HandleError(null, ex);
	}


}
