WARLORD

A multiplayer game for up to 4 people, vaguely based on the old Atari game "Warlord".

By Steve Smith - https://twitter.com/stephencsmith


STARTING A GAME
At least 2 players must join before the game starts.


CONTROLS
Player 1 is Keyboard (arrow keys).
All other players use gamepads plugged into USB ports - please plug them in *before* you run the game.


CREDITS
Design and programming by Steve Smith - https://twitter.com/stephencsmith/ 

Source code licensed under MIT Licence.

